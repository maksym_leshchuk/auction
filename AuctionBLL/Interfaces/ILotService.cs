﻿using AuctionBLL.Models;
using AutoMapper;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AuctionBLL.Interfaces
{
    public interface ILotService
    {
        IMapper Mapper { get; set; }

        Task AddAsync(LotModel model);
        void Delete(LotModel model);
        Task DeleteByIdAsync(int id);
        IEnumerable<LotModel> GetAll();
        Task<LotModel> GetByIdAsync(int id);
        Task Update(LotModel model);
    }
}