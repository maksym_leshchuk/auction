﻿using AuctionDAL.InterafacesRepositories;
using System.Threading.Tasks;

namespace AuctionDAL.UnitOfWork
{
    public interface IUnitOfWork
    {
        IBidRepository BidRepository { get; set; }
        ILotRepository LotRepository { get; set; }
        IUserRepository UserRepository { get; set; }

        public Task SaveAsync();
    }
}