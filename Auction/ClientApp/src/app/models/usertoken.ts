import { DecimalPipe } from "@angular/common";

export class UserToken {
  username: string;
  token: string;


  constructor(username: string, token: string) {

    this.username = username;
    this.token = token;

  }


}
