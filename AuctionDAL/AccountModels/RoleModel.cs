﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AuctionDAL.AccountModels
{
    public class RoleModel
    {
        [Required]
        [MinLength(3)]
        public string RoleName { get; set; }
    }
}
