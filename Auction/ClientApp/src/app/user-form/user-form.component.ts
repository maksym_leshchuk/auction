import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../models/user';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  form: FormGroup;
  user: User;
  constructor(private _userService: UserService, private router:Router) {
    this._userService.profile(localStorage.getItem("username")).subscribe(user => this.user = user);
    this.form = new FormGroup(
      {
        'firstName': new FormControl(''),
        'middleName': new FormControl(''),
        'lastName': new FormControl(''),
        'phone': new FormControl('', [Validators.pattern("^[0-9]*$")]),
        'address': new FormControl('')
      }
    )
  }

  ngOnInit() {
    //this._userService.profile(localStorage.getItem("username")).subscribe(user => this.user = user);
  }

  edit() {
    var username = this.user.userName;
    var email = this.user.email;
    var firstname = this.form.get('firstName').value;
    var middlename = this.form.get('middleName').value;
    var lastname = this.form.get('lastName').value;
    var phone = this.form.get('phone').value;
    var address = this.form.get('address').value;

    var user = new User(username, email, firstname, middlename, lastname, phone, address);

    this._userService.edit(user).subscribe(user => this.user = user);
    this.router.navigate(['/profile']).then(() => {
      window.location.reload();
    });
  }

}
