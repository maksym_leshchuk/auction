﻿using AuctionBLL.Interfaces;
using AuctionBLL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Auction.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class BidController : Controller
    {
        public IBidService _bidService { get; set; }
        public ILotService _lotService { get; set; }


        public BidController(IBidService bidService, ILotService lotService)
        {
            this._bidService = bidService;
            this._lotService = lotService;
        }

        /// <summary>
        /// Get bids for certain lot
        /// </summary>
        /// <param name="LotId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("lot/{LotId}")]
        public IActionResult GetForLot(int LotId)
        {
            return Ok(_bidService.GetAll().Where(x => x.LotId == LotId));
        }

        /// <summary>
        /// Get all bids
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_bidService.GetAll());
        }

        /// <summary>
        /// Get bids for User
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("user/{UserId}")]
        public IActionResult GetForUser(int UserId)
        {
            return Ok(_bidService.GetAll().Where(x => x.UserId == UserId));
        }

        /// <summary>
        /// Delete bid by id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpDelete]
        public async Task<IActionResult> Delete(int Id)
        {
            try
            {
                await _bidService.DeleteByIdAsync(Id);
                return Ok();
            }
            catch
            {
                return BadRequest();
            }

        }


        /// <summary>
        /// Create new bid
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] BidModel bid)
        {

            try
            {

                var lastBid = _bidService.GetAll().Where(x => x.LotId == bid.LotId).OrderBy(x => x.Price).Last();
                if (lastBid != null && lastBid.Price > bid.Price)
                {
                    return BadRequest("Price of new bid must be greater than the last bid's");
                }


            }
            catch
            {
                if ((await _lotService.GetByIdAsync(bid.LotId)).StartingPrice > bid.Price)
                {
                    return BadRequest("Price of the first bid must be greater than the starting price");
                }
            }

            await _bidService.AddAsync(new BidModel()
            {
                UserId = bid.UserId,
                LotId = bid.LotId,
                Price = bid.Price,
                CreationDate = DateTime.Now
            });


            return Created("", "");
        }

    }
}
