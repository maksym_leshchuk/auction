import { Route } from '@angular/compiler/src/core';
import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Bid } from '../models/bid';
import { User } from '../models/user';
import { BidService } from '../services/bid.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-bid-form',
  templateUrl: './bid-form.component.html',
  styleUrls: ['./bid-form.component.css']
})
export class BidFormComponent implements OnInit {
  private _sub: Subscription = new Subscription();
  @Input()
  lotId: number;

  users: User[];

  form: FormGroup;

  constructor(private _bidService: BidService, private _userService:UserService,  private router: Router) {

    this._sub.add(this._userService.getAll().subscribe(users => this.users = users));

    this.form = new FormGroup(
      {
        'price': new FormControl('', Validators.required),
        
      }
    )
  }

  ngOnInit() {
    
  }

  create() {
    const price: number = + this.form.get('price').value;
    console.log(localStorage.getItem('username'))
    console.log(this.users)
    const bid = new Bid(this.lotId, this.users.filter(x => x.userName.toLowerCase() == localStorage.getItem('username').toLowerCase())[0].userId , price);
    this._bidService.postBid(bid).subscribe(bid => bid);
    window.location.reload();
   
  }

}
