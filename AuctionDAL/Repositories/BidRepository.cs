﻿using AuctionDAL.InterafacesRepositories;
using AuctionDAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionDAL.Repositories
{
    public class BidRepository : IBidRepository 
    {
        private AuctionDBContext auctionDBContext;

        public BidRepository(AuctionDBContext auctionDBContext)
        {
            this.auctionDBContext = auctionDBContext;
        }

        public async Task AddAsync(Bid entity)
        {
            await auctionDBContext.Bids.AddAsync(entity);
        }

        public async Task Delete(Bid entity)
        {
            auctionDBContext.Bids.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var item = await auctionDBContext.Bids.FindAsync(id);
            auctionDBContext.Bids.Remove(item);
        }

        public IQueryable<Bid> GetAll()
        {
            return auctionDBContext.Bids.AsQueryable();
        }

        public async Task<Bid> GetByIdAsync(int id)
        {
            return await auctionDBContext.Bids.FindAsync(id);
        }

        public void Update(Bid entity)
        {
            

            auctionDBContext.Attach(entity);
            
            auctionDBContext.SaveChanges();
        }
    }
}
