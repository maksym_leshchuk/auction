import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor() { }

  readCookie(name: string) {
    
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') c = c.substring(1, c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
  }

  getToken() {
    const jwt = this.readCookie(".AspNetCore.Application.Id");

    let jwtData = jwt.split('.')[1]
    console.log('jwtData: ' + jwtData)
    let decodedJwtJsonData = window.atob(jwtData)
    console.log('decodedJwtJsonData: ' + decodedJwtJsonData)
    let decodedJwtData = JSON.parse(decodedJwtJsonData)

    let role = decodedJwtData["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"];
    console.log(role)

    console.log('decodedJwtData: ' + decodedJwtData)
    return decodedJwtData;
  }
}
