﻿using AuctionBLL.Interfaces;
using AuctionBLL.Models;
using AuctionDAL.AccountModels;
using AuctionDAL.Models;
using AuctionDAL.UnitOfWork;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionBLL.Services
{
    /// <summary>
    /// Service for Users Repository
    /// </summary>
    public class UserService : IUserService
    {
        private IUnitOfWork unitOfWork;
        private readonly UserManager<ApplicationUser> _userManager;
        public IMapper Mapper { get; set; }
        public UserService(IUnitOfWork unitOfWork, IMapper mapper, UserManager<ApplicationUser> userManager)
        {
            this.unitOfWork = unitOfWork;
            this.Mapper = mapper;
            this._userManager = userManager;
        }
        public IEnumerable<UserModel> GetAll()
        {
            var temp = unitOfWork.UserRepository.GetAll();
            return Mapper.Map<IEnumerable<UserModel>>(temp);
        }
        public async Task<UserModel> GetByIdAsync(int id)
        {
            
                return Mapper.Map<UserModel>(await unitOfWork.UserRepository.GetByIdAsync(id));
            
        }

        public async Task<UserModel> GetByNameAsync(string name)
        {

            return Mapper.Map<UserModel>(await unitOfWork.UserRepository.GetAll().FirstOrDefaultAsync(x => x.UserName == name));

        }

        public async Task UpdateProfile(UserModel model)
        {
            var user = unitOfWork.UserRepository.GetAll().FirstOrDefault(x => x.UserName == model.UserName && x.Email == model.Email);
            if(model.FirstName !=null || model.FirstName.Length != 0)
            {
                user.FirstName = model.FirstName;
            }

            if (model.MiddleName != null && model.MiddleName.Length != 0)
            {
                user.MiddleName = model.MiddleName;
            }
            if (model.LastName != null && model.LastName.Length != 0)
            {
                user.LastName = model.LastName;
            }
            if (model.Adress != null && model.Adress.Length != 0)
            {
                user.Adress = model.Adress;
            }
            if (model.Phone != null && model.Phone.Length != 0)
            {
                user.Phone = model.Phone;
            }

            unitOfWork.UserRepository.Update(user);
            await unitOfWork.SaveAsync();
        }

        public async Task AddAsync(UserModel model)
        {
            await unitOfWork.UserRepository.AddAsync(Mapper.Map<User>(model));
            await unitOfWork.SaveAsync();
        }

        public async Task Delete(string username)
        {
            await unitOfWork.UserRepository.Delete(unitOfWork.UserRepository.GetAll().FirstOrDefault(x => x.UserName == username));
            await _userManager.DeleteAsync(_userManager.Users.FirstOrDefault(x => x.UserName == username));
            
        }
        public async void Delete(UserModel model)
        {
            await unitOfWork.UserRepository.Delete(Mapper.Map<User>(model));
            await unitOfWork.SaveAsync();
        }
        public async Task DeleteByIdAsync(int id)
        {
            await unitOfWork.UserRepository.DeleteByIdAsync(id);
            await unitOfWork.SaveAsync();
        }
        public async Task Update(UserModel model)
        {

            unitOfWork.UserRepository.Update(Mapper.Map<User>(model));
            await unitOfWork.SaveAsync();
        }
    }
}
