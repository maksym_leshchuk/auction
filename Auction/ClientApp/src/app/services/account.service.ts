import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Login } from '../models/login';
import { Register } from '../models/register';
import { User } from '../models/user';
import { UserToken } from '../models/usertoken';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private backend = 'https://localhost:44301/Account'

  constructor(private http: HttpClient) { }

  login(login: Login): Observable<UserToken> {

    return this.http.post<UserToken>(`${this.backend}/login`, login);
    
  }


  register(user: Register): Observable<any> {
    return this.http.post<Register>(`${this.backend}/register`, user)
  }

  logout(): Observable<any> {

    return this.http.delete<any>(`${this.backend}/logout`);

  }
}
