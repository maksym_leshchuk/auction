import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from '../services/account.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private router: Router, private _accountService: AccountService) { }

  ngOnInit() {
    localStorage.setItem("username", undefined);
    localStorage.setItem("token", undefined);
    this._accountService.logout().subscribe();
  }

}
