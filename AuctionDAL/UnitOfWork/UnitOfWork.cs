﻿using AuctionDAL.InterafacesRepositories;
using AuctionDAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AuctionDAL.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(AuctionDBContext auctionDBContext)
        {
            AuctionDBContext = auctionDBContext;
            UserRepository = new UserRepository(auctionDBContext);
            BidRepository = new BidRepository(auctionDBContext);
            LotRepository = new LotRepository(auctionDBContext);
            
        }
        private AuctionDBContext AuctionDBContext;
        public IUserRepository UserRepository { get; set; }
        public IBidRepository BidRepository { get; set; }
        public ILotRepository LotRepository { get; set; }
        

        public async Task SaveAsync()
        {
            await AuctionDBContext.SaveChangesAsync();
        }
    }
}
