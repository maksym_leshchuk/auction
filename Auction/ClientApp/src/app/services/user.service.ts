import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private user: User;

  private backend = 'https://localhost:44301/User'

  constructor(private http: HttpClient) { }
  profile(name: string): Observable<User> {
    let params = new HttpParams();
    params = params.append('name', name);
    return this.http.get<User>(`${this.backend}/profile`, { params: params });
  }

  getAll(): Observable<User[]> {
    
    return this.http.get<User[]>(`${this.backend}`);
  }

  edit(user: User) {
    
    return this.http.put<User>(`${this.backend}`, user);
  }

  delete(name: string) {
    let params = new HttpParams();
    params = params.append('username', name);

    return this.http.delete<User>(`${this.backend}`, { params: params });
  }
}
