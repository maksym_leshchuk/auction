﻿using AuctionDAL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AuctionBLL.Models
{
    public class LotModel
    {
        public int LotId { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        [MinLength(10)]
        [MaxLength(1000)]
        public string Description { get; set; }

        [Column(TypeName = "datetime2(7)")]
        public DateTime CreationDate { get; set; }

        [Column(TypeName = "decimal(10, 2)")]
        public decimal StartingPrice { get; set; }
        public LotModel()
        {
            CreationDate = DateTime.Now;
        }
    }
}
