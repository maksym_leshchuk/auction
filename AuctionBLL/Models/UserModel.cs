﻿using AuctionDAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AuctionBLL.Models
{
    public class UserModel 
    {   
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public string Phone { get; set; }
        public string Adress { get; set; }
    }
}
