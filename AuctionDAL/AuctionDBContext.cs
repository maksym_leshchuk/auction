﻿using AuctionDAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace AuctionDAL
{
    public class AuctionDBContext : DbContext
    {

        public AuctionDBContext(DbContextOptions<AuctionDBContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
        

        public DbSet<User> Users { get; set;  }
        public DbSet<Bid> Bids { get; set; }
        public DbSet<Lot> Lots { get; set; }
        


    }
}
