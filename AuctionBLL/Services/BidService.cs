﻿using AuctionBLL.Interfaces;
using AuctionBLL.Models;
using AuctionDAL.Models;
using AuctionDAL.UnitOfWork;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionBLL.Services
{
    /// <summary>
    /// Service for Bids Repository
    /// </summary>
    public class BidService : IBidService
    {
        private IUnitOfWork unitOfWork { get; set; }
        public IMapper Mapper { get; set; }

        
        public BidService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            Mapper = mapper;
        }
        public IEnumerable<BidModel> GetAll()
        {
            return Mapper.Map<IEnumerable<BidModel>>(unitOfWork.BidRepository.GetAll());
        }
        public async Task<BidModel> GetByIdAsync(int id)
        {
            return Mapper.Map<BidModel>(await unitOfWork.BidRepository.GetByIdAsync(id));
        }
        public async Task AddAsync(BidModel model)
        {
            
            await unitOfWork.BidRepository.AddAsync(Mapper.Map<Bid>(model));
            
            await unitOfWork.SaveAsync();
        }
        public async void Delete(BidModel model)
        {
            await unitOfWork.BidRepository.Delete(Mapper.Map<Bid>(model));
            await unitOfWork.SaveAsync();
        }
        public async Task DeleteByIdAsync(int id)
        {
            await unitOfWork.BidRepository.DeleteByIdAsync(id);
            await unitOfWork.SaveAsync();
        }
        public async Task Update(BidModel model)
        {
            unitOfWork.BidRepository.Update(Mapper.Map<Bid>(model));
            await unitOfWork.SaveAsync();
        }

    }
}
