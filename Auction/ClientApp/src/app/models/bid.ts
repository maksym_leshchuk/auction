import { DecimalPipe } from "@angular/common";

export class Bid {
  bidId: number;
  lotId: number;
  userId: number;
  price: number;
  creationDate: Date;

  constructor( lotId: number, userId: number, price: number) {
    this.lotId = lotId;
    
    this.userId = userId;
    this.price = price;
   
  }
}
