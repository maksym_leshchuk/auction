import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Route, Router } from "@angular/router";
import { debug } from "console";
import { Observable } from "rxjs";
import { AccountService } from "../services/account.service";


@Injectable()
export class AuthInterceptor implements HttpInterceptor{

  constructor(private router: Router) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

      const cred = localStorage.getItem("token");
      
      if (!cred) {
        this.router.navigateByUrl('/login');
        return;
      }

      const newRequest = req.clone({ headers: req.headers.set("Authorization", "Bearer " + cred) });
      return next.handle(newRequest);
    }
}
