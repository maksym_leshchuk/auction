﻿using AuctionBLL.Interfaces;
using AuctionBLL.Models;
using AuctionDAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auction.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class LotController : Controller
    {
        private ILotService _lotService;
        public LotController(ILotService lotService)
        {
            _lotService = lotService;
        }

        /// <summary>
        /// Create new lot
        /// </summary>
        /// <param name="lot"></param>
        /// <returns></returns>
        [Authorize(Roles ="admin, contributor")]
        [HttpPost]
        public async Task<IActionResult> Create([FromBody]LotModel lot)
        {


            await _lotService.AddAsync(new LotModel()
            {
                UserId = lot.UserId,
                Name = lot.Name,
                StartingPrice = lot.StartingPrice,
                Description = lot.Description,
                CreationDate = DateTime.Now
            });

            return Created("", "");
        }

        /// <summary>
        /// Get all lots
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            var i = User.IsInRole("admin");

            return Ok(_lotService.GetAll());
        }
        

        /// <summary>
        /// Get lot by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var i = User;
            return Ok(await _lotService.GetByIdAsync(id));
        }

        /// <summary>
        /// Delete lot by id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [Authorize(Roles = "admin")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int Id)
        {
            await _lotService.DeleteByIdAsync(Id);
            return Ok();
        }
    }
}
