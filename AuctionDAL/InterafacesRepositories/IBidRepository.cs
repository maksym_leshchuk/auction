﻿using AuctionDAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AuctionDAL.InterafacesRepositories
{
    public interface IBidRepository : IRepository<Bid>
    {
    }
}
