import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { isNullOrUndefined } from 'util';
import { Bid } from '../models/bid';
import { Lot } from '../models/lot';
import { User } from '../models/user';
import { BidService } from '../services/bid.service';
import { LotsService } from '../services/lots.service';
import { TokenService } from '../services/token.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-lot-details',
  templateUrl: './lot-details.component.html',
  styleUrls: ['./lot-details.component.css']
})
export class LotDetailsComponent implements OnInit {
  private _sub: Subscription = new Subscription();
  @Input()
  lot: Lot;
  bids: Bid[];
  users: User[];
  hasAccess: boolean;
  role: string;

  constructor(private _lotService: LotsService, private _bidService: BidService, private route: ActivatedRoute, private _userService: UserService, private router:Router, private _tokenService: TokenService) { }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    if (isNullOrUndefined(id)) {
      return;
    }
    this._sub.add(this._lotService.getLot(id).subscribe(lot => this.lot = lot));
    this._sub.add(this._userService.getAll().subscribe(users => this.users = users));

    this._sub.add(this._bidService.getBidsForLot(id).subscribe(bids => this.bids = bids.sort(
      (a, b) => {
        var price1: number = + a.price;
        var price2: number = + b.price;

            return 0 - price1 > price2 ? 1 : -1;
        })
    ));
    this.role = this._tokenService.getToken()["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"];
    
  }
  getUserName(userId: number): string {
    return this.users.filter(x => x.userId == userId)[0].userName;
  }

  deleteLot(id: number) {
    this._lotService.deleteLot(id).subscribe(lot => lot);
    this.router.navigate(['/lots']).then(() => {
      window.location.reload();
    });
  }

  ngOnDestroy(): void {
    this._sub.unsubscribe();
  }

}
