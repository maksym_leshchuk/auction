import { Component, OnInit } from '@angular/core';
import { ReactiveFormsModule, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Lot } from '../models/lot';
import { User } from '../models/user';
import { LotsService } from '../services/lots.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-lot-form',
  templateUrl: './lot-form.component.html',
  styleUrls: ['./lot-form.component.css']
})
export class LotFormComponent implements OnInit {
  private _sub: Subscription = new Subscription();
  users: User[];
  form: FormGroup;
  constructor(private _lotService: LotsService, private _userService: UserService, private router: Router) {

    this._sub.add(this._userService.getAll().subscribe(users => this.users = users));

    this.form = new FormGroup(
      {
        'name': new FormControl('', Validators.required),
        'description': new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(1000)]),
        'startingPrice': new FormControl('', Validators.required)
      }
    )
  }

  ngOnInit() {
  }

  create() {
    const name = this.form.get('name').value;
    const description = this.form.get('description').value;
    const startingPrice: number =+ this.form.get('startingPrice').value;
    const lot = new Lot(name, description, startingPrice);
    console.log(this.users.filter(x => x.userName == localStorage.getItem('username')))
    lot.userId = this.users.filter(x => x.userName.toLowerCase() == localStorage.getItem('username').toLowerCase())[0].userId;
    console.log(lot.userId)



    this._lotService.postLot(lot).subscribe(lot => lot);

    this.router.navigate(['/lots']).then(() => {
      window.location.reload();
    });
  }

}
