﻿
using AuctionBLL.Interfaces;
using AuctionBLL.Models;
using AuctionDAL.AccountModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Auction.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : Controller
    {

        public IUserService _userService { get; set; }
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_userService.GetAll());
        }

        /// <summary>
        /// Get user information by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet("profile")]
        public async Task<IActionResult> Get(string name)
        {
            return Ok(await _userService.GetByNameAsync(name));
        }

        /// <summary>
        /// U[date user information
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPut]
        public IActionResult UpdateProfile([FromBody]UserModel model)
        {
            try
            {
                _userService.UpdateProfile(model);

                return Ok("Updated");
            }
            catch
            {
                return BadRequest();
            }
        }


        /// <summary>
        /// Delete user
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        [Authorize(Roles ="admin")]
        [HttpDelete]
        public IActionResult Delete(string username)
        {
            try
            {
                _userService.Delete(username);

                return Ok("Updated");
            }
            catch
            {
                return BadRequest();
            }
        }

        



    }
}
