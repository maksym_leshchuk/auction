﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AuctionDAL.Models
{
    public class Lot
    {
        [Key]
        public int LotId { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        [MinLength(10)]
        [MaxLength(1000)]
        public string Description { get; set; }
        
        [Column(TypeName = "datetime2(7)")]
        public DateTime CreationDate { get; set; }

        [Column(TypeName = "decimal(10, 2)")]
        public decimal StartingPrice { get; set; }

        public ICollection<Bid> Bids { get; set; }
        public User User { get; set; }
    }
}
