﻿using AuctionDAL.InterafacesRepositories;
using AuctionDAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionDAL.Repositories
{
    public class UserRepository : IUserRepository
    {
        private AuctionDBContext auctionDBContext;

        public UserRepository(AuctionDBContext auctionDBContext)
        {
            this.auctionDBContext = auctionDBContext;
        }

        public async Task AddAsync(User entity)
        {
            await auctionDBContext.Users.AddAsync(entity);
        }

        public async Task Delete(User entity)
        {
            
            auctionDBContext.Users.Remove(entity);
            await auctionDBContext.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            var item = await auctionDBContext.Users.FindAsync(id);
            auctionDBContext.Users.Remove(item);
        }

        public IQueryable<User> GetAll()
        {
            
             return auctionDBContext.Users.AsQueryable();
            
            
        }

        public async Task<User> GetByIdAsync(int id)
        {
            return await auctionDBContext.Users.FindAsync(id);
        }

        public async void Update(User entity)
        {
            auctionDBContext.Attach(entity);
            auctionDBContext.Attach<User>(entity).State = EntityState.Modified;

            auctionDBContext.SaveChanges();
        }
    }
}
