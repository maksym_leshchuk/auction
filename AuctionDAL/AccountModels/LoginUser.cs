﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AuctionDAL.AccountModels
{
    public class LoginUser
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
