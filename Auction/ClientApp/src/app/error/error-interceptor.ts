import
{ Injectable }
from '@angular/core';
import
{ HttpRequest, HttpHandler, HttpEvent, HttpInterceptor }
from '@angular/common/http';
import
{ Observable, throwError }
from 'rxjs';
import
{ catchError }
from 'rxjs/operators';
import { AccountService } from '../services/account.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor
{
  constructor() { }

intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
  return next.handle(request).pipe(catchError(err => {
    if ([ 401, 403].indexOf(err.status) !== -1)
    {
      alert("Unauthorized");
      location.reload();
    }

    if ([400].indexOf(err.status) !== -1) {
      alert("Invalid input!");
      location.reload();
    }

    if ([500].indexOf(err.status) !== -1)
    {

      alert("Username or Password are incorrect!");
      location.reload();
    }

    const error = err.error.message || err.statusText;
    return throwError(error);
  }))
    }
}
