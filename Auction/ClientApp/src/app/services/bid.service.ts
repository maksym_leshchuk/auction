import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Bid } from '../models/bid';
import { Lot } from '../models/lot';

@Injectable({
  providedIn: 'root'
})
export class BidService {

  private backend = 'https://localhost:44301/Bid'

  constructor(private http: HttpClient) { }

  getBidsForLot(id: number): Observable<Bid[]> {
    return this.http.get<Bid[]>(`${this.backend}/lot/${id}`);
  }

  getLot(id: number): Observable<Bid> {
    return this.http.get<Bid>(`${this.backend}/${id}`);
  }

  postBid(bid: Bid): Observable<Bid> {
    
    return this.http.post<Bid>(this.backend, bid);
  }
}
