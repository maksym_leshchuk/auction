import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { error } from 'protractor';
import { Login } from '../models/login';
import { UserToken } from '../models/usertoken';
import { AccountService } from '../services/account.service';
import { Subscription } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Register } from '../models/register';

@Component({
  selector: 'app-login',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  form: FormGroup;
  username: string;
  password: string;
  email: string;
  firstName: string;
  lastName: string;
  middleName: string;
  phone: string;
  adress: string;

  error: Response;
  

  constructor(private router: Router, private _accountService: AccountService) {
    this.form = new FormGroup(
      {
        'username': new FormControl('', [Validators.required, Validators.minLength(5)]),
        'email': new FormControl('', [Validators.required, Validators.email]),
        'password': new FormControl('', Validators.required),
        'firstName': new FormControl('', Validators.required),
        'middleName': new FormControl(''),
        'lastName': new FormControl('', Validators.required),
        'phone': new FormControl('', [ Validators.pattern("^[0-9]*$")]),
        'address': new FormControl('')
      }
    )
  }

  ngOnInit() {

  }

  submit() {
    this.username = this.form.get("username").value;
    this.email = this.form.get("email").value;
    this.password = this.form.get("password").value;
    this.firstName = this.form.get("firstName").value;
    this.middleName = this.form.get("middleName").value;
    this.lastName = this.form.get("lastName").value;
    this.phone = this.form.get("phone").value;
    this.adress = this.form.get("address").value;
    console.log(this.email)
    var register = new Register(this.username, this.password, this.email, this.firstName, this.middleName, this.lastName, this.phone, this.adress);


    var i;
    this._accountService.register(register).subscribe(res => i = res, (error: Response) => {this.error = error; });
    this.router.navigate(['/login'])
  }
}
