import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  user: User;

  constructor(private _userService: UserService) { }

  ngOnInit() {
    this._userService.profile(localStorage.getItem("username")).subscribe(user => this.user = user);

  }

}
