import { Component, OnInit } from '@angular/core';
import { Lot } from '../models/lot';
import { LotsService } from '../services/lots.service';
import { TokenService } from '../services/token.service';

@Component({
  selector: 'app-lots-list',
  templateUrl: './lots-list.component.html',
  styleUrls: ['./lots-list.component.css']
})
export class LotsListComponent implements OnInit {
  lots: Lot[];
  searchString: string;
  private allLots: Lot[];
  selectedLot: Lot;

  role: string;

  constructor(private _lotService: LotsService, private _tokenService: TokenService) { }

  ngOnInit() {
    this._lotService.getLots().subscribe(lots => this.lots = lots);

    this.role = this._tokenService.getToken()["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"];
  }

 

  onSearch() {
    if (!this.allLots) {
      this.allLots = this.lots;
    }

    if (!this.searchString ) {
      if (this.allLots.length !== this.lots.length) {
        this.lots = this.allLots;
      }
      return;
    }

    this.lots = this.lots.filter(x => x.name.includes(this.searchString));
    if (this.lots.length == 0) {
      alert("No item found");
      this.lots = this.allLots;

    }
  }

  viewLot(lot: Lot) {
    this.selectedLot = lot;
  }

}
