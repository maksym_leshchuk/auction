import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Lot } from '../models/lot';
import { map } from 'rxjs/operators'
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class LotsService {
  private backend = 'https://localhost:44301/Lot'

  constructor(private http: HttpClient) { }

  getLots(): Observable<Lot[]>{
    return this.http.get<Lot[]>(this.backend);
  }

  getLot(id: number): Observable<Lot> {
    return this.http.get<Lot>(`${this.backend}/${id}`);
  }

  postLot(lot: Lot): Observable<Lot> {
    return this.http.post<Lot>(this.backend, lot);
  }

  deleteLot(id: number): Observable<Lot> {
    return this.http.delete<any>(`${this.backend}/${id}`);
  }
}
