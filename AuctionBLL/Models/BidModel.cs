﻿using AuctionDAL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AuctionBLL.Models
{
    public class BidModel
    {
        public int BidId { get; set; }
        public int UserId { get; set; }
        public int LotId { get; set; }
        [Column(TypeName = "decimal(10, 2)")]
        public decimal Price { get; set; }
        [Column(TypeName = "datetime2(7)")]
        public DateTime CreationDate { get; set; }

        public BidModel()
        {
            CreationDate = DateTime.Now;
        }
    }
}
