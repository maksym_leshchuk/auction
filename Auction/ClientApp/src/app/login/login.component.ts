import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { error } from 'protractor';
import { Login } from '../models/login';
import { UserToken } from '../models/usertoken';
import { AccountService } from '../services/account.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private _sub: Subscription = new Subscription();
  username: string;
  password: string;
  userToken: UserToken;

  constructor(private router: Router, private _accountService: AccountService) { }

  ngOnInit() {
    
  }

 submit() {
    const cred: Login = new Login(this.username, this.password);

    this._accountService.login(cred).subscribe(token => this.userToken = token); 
    
   console.log(this.username)
   localStorage.setItem("username", this.username);
    
    this.router.navigate(['/']);
  }

  ngOnDestroy() {
    this._sub.unsubscribe();
  }

  
}
