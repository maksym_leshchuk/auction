﻿using AuctionDAL.AccountModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionBLL.ServicesAdministration
{
    /// <summary>
    /// Account Service class 
    /// (login, register and role managment)
    /// </summary>
    public class AccountService : IAccountService
    {
        
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        

        public AccountService(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            
        }

        public async Task Register(RegisterUser user)
        {
            var result = await _userManager.CreateAsync(
                new ApplicationUser
                {
                    Email = user.Email,
                    UserName = user.UserName,
                    FirstName = user.FirstName,
                    LastName = user.LastName
                },
                user.Password);

            if (!result.Succeeded)
            {
                throw new System.Exception(string.Join(';', result.Errors.Select(x => x.Description)));
            }
        }

        public async Task<ApplicationUser> Login(LoginUser model)
        {
            var user = _userManager.Users.SingleOrDefault(u => u.UserName == model.Username);
            if (user is null)
            {
                return null;
            }

            if(await _userManager.CheckPasswordAsync(user, model.Password))
            {
                return user;
            }
            else
            {
                return null;
            }
            
        }

        

        public async Task AssignUserToRoles(AssignedRoles assignedRoles)
        {
            var user = _userManager.Users.SingleOrDefault(u => u.UserName == assignedRoles.UserName);
            var roles = _roleManager.Roles
                .ToList()
                .Where(r => assignedRoles.Roles.Contains(r.Name, StringComparer.OrdinalIgnoreCase))
                .Select(r => r.NormalizedName)
                .ToList();

            var result = await _userManager.AddToRolesAsync(user, roles); 

            if (!result.Succeeded)
            {
                throw new System.Exception(string.Join(';', result.Errors.Select(x => x.Description)));
            }
        }

        public async Task CreateRole(string roleName)
        {
           await _roleManager.CreateAsync(new IdentityRole(roleName));
        }

        public async Task<IEnumerable<string>> GetRolesForCertainUser(ApplicationUser user)
        {
            return (await _userManager.GetRolesAsync(user)).ToList();
        }

        public async Task<IEnumerable<IdentityRole>> GetRoles()
        {
            return await _roleManager.Roles.ToListAsync();
        }

        public async Task DeleteRole(string roleName)
        {
            await _roleManager.DeleteAsync((await _roleManager.Roles.ToListAsync()).FirstOrDefault(x => x.Name == roleName));
        }
    }
}
