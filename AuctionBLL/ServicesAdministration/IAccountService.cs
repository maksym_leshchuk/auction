﻿using AuctionDAL.AccountModels;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AuctionBLL.ServicesAdministration
{
    public interface IAccountService
    {
        Task Register(RegisterUser model);
        Task<ApplicationUser> Login(LoginUser model);
        Task<IEnumerable<IdentityRole>> GetRoles();
        Task CreateRole(string roleName);
        Task DeleteRole(string roleName);
        Task AssignUserToRoles(AssignedRoles assignedRoles);
        Task<IEnumerable<string>> GetRolesForCertainUser(ApplicationUser user);
    }
}
