﻿using AuctionBLL.Models;
using AuctionDAL.AccountModels;
using AutoMapper;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AuctionBLL.Interfaces
{
    public interface IUserService
    {
        IMapper Mapper { get; set; }

        Task AddAsync(UserModel model);
        void Delete(UserModel model);
        Task Delete(string username);
        Task DeleteByIdAsync(int id);
        IEnumerable<UserModel> GetAll();
        Task<UserModel> GetByIdAsync(int id);
        Task<UserModel> GetByNameAsync(string name);
        Task Update(UserModel model);
        Task UpdateProfile(UserModel model);
    }
}