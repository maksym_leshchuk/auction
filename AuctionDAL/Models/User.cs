﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace AuctionDAL.Models
{
    
    public class User
    {
        [Key]
        public int UserId { get; set; }
        [MinLength(4)]
        public string UserName { get; set; }
        [EmailAddress]
        public string Email { get; set; }

        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Adress { get; set; }

        
        public ICollection<Bid> Bids { get; set; }
        public ICollection<Lot> Lots { get; set; }
    }
}
