﻿using AuctionDAL.InterafacesRepositories;
using AuctionDAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuctionDAL.Repositories
{
    public class LotRepository : ILotRepository
    {
        private AuctionDBContext auctionDBContext;

        public LotRepository(AuctionDBContext auctionDBContext)
        {
            this.auctionDBContext = auctionDBContext;
        }

        public async Task AddAsync(Lot entity)
        {
            await auctionDBContext.Lots.AddAsync(entity);
        }

        public async Task Delete(Lot entity)
        {
            auctionDBContext.Lots.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var item = await auctionDBContext.Lots.FindAsync(id);
            auctionDBContext.Lots.Remove(item);
        }

        public IQueryable<Lot> GetAll()
        {
            return auctionDBContext.Lots.AsQueryable();
        }

        public async Task<Lot> GetByIdAsync(int id)
        {
            return await auctionDBContext.Lots.FindAsync(id);
        }

        public async void Update(Lot entity)
        {
            var item = auctionDBContext.Lots.Attach(entity);
            auctionDBContext.Entry(entity).State = EntityState.Modified;
        }
    }
}
