﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AuctionDAL.AccountModels
{
    public class AssignedRoles
    {
        public string UserName { get; set; }
        public string[] Roles { get; set; }
    }
}
