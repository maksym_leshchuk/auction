﻿using AuctionBLL.Interfaces;
using AuctionBLL.Models;
using AuctionDAL.Models;
using AuctionDAL.UnitOfWork;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AuctionBLL.Services
{
    /// <summary>
    /// Service for Lots Repository
    /// </summary>
    public class LotService : ILotService
    {
        private IUnitOfWork unitOfWork { get; set; }
        public IMapper Mapper { get; set; }
        public LotService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            Mapper = mapper;
        }
        public IEnumerable<LotModel> GetAll()
        {
            return Mapper.Map<IEnumerable<LotModel>>(unitOfWork.LotRepository.GetAll());
        }
        public async Task<LotModel> GetByIdAsync(int id)
        {
            return Mapper.Map<LotModel>(await unitOfWork.LotRepository.GetByIdAsync(id));
        }
        public async Task AddAsync(LotModel model)
        {
            var item = Mapper.Map<Lot>(model);
            await unitOfWork.LotRepository.AddAsync(item);
            await unitOfWork.SaveAsync();
        }
        public async void Delete(LotModel model)
        {
            await unitOfWork.LotRepository.Delete(Mapper.Map<Lot>(model));
            await unitOfWork.SaveAsync();
        }
        public async Task DeleteByIdAsync(int id)
        {
            await unitOfWork.LotRepository.DeleteByIdAsync(id);
            await unitOfWork.SaveAsync();
        }
        public async Task Update(LotModel model)
        {
            unitOfWork.LotRepository.Update(Mapper.Map<Lot>(model));
            await unitOfWork.SaveAsync();
        }
    }
}
