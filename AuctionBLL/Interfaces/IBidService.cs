﻿using AuctionBLL.Models;
using AutoMapper;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AuctionBLL.Interfaces
{
    public interface IBidService
    {
        IMapper Mapper { get; set; }

        Task AddAsync(BidModel model);
        void Delete(BidModel model);
        Task DeleteByIdAsync(int id);
        IEnumerable<BidModel> GetAll();
        Task<BidModel> GetByIdAsync(int id);
        Task Update(BidModel model);
    }
}