﻿using System;
using System.Collections.Generic;
using System.Text;
using AuctionDAL.AccountModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace AuctionDAL
{
    public class AdministrationDBContext : IdentityDbContext<ApplicationUser>
    {
        public AdministrationDBContext(DbContextOptions<AdministrationDBContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<IdentityRole>();
        }
    }
}
