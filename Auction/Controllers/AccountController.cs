﻿using Auction.Helpers;
using AuctionBLL.Interfaces;
using AuctionBLL.Models;
using AuctionBLL.ServicesAdministration;
using AuctionDAL.AccountModels;
using AuctionDAL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Auction.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AccountController : Controller
    {
        private readonly IAccountService AccountService;
        private IUserService UserService;
        private readonly JwtSettings JwtSettings;
        public AccountController(IAccountService accountService, IOptionsSnapshot<JwtSettings> jwtSettings, IUserService UserService)
        {
            AccountService = accountService;
            JwtSettings = jwtSettings.Value;
            this.UserService = UserService;
        }


        /// <summary>
        /// Register new user
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody]RegisterUser model)
        {
            await AccountService.Register(new RegisterUser
            {
                Email = model.Email,
                UserName = model.UserName,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Password = model.Password
            });
            await UserService.AddAsync(new UserModel
            {
                UserName = model.UserName,
                Email = model.Email,
                FirstName = model.FirstName,
                MiddleName = model.MiddleName,
                LastName = model.LastName,
                Adress = model.Adress,
                Phone = model.Phone

            }) ;
        
            return Created("", "");
        }


        /// <summary>
        /// Login user into session
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginUser model)
        {
            var user = await AccountService.Login(new LoginUser
            {
                Username = model.Username,
                Password = model.Password
            });

            if (user is null)
            {
                return Unauthorized("Wrong username or password");
            }

            var roles = await AccountService.GetRolesForCertainUser(user);
            var token = JwtHelper.GenerateJwt(user, roles, JwtSettings);
            
            HttpContext.Response.Cookies.Append(".AspNetCore.Application.Id", token,
            new CookieOptions
            {
                MaxAge = TimeSpan.FromMinutes(60)
            });
            
            return Ok(new UserToken { Username = model.Username, Token = token});

            //return Ok("Success");
        }



        /// <summary>
        /// Logout user from session
        /// </summary>
        /// <returns></returns>
        [HttpDelete("logout")]
        public async Task<IActionResult> Logout()
        {
            HttpContext.Response.Cookies.Delete(".AspNetCore.Application.Id");
            
            return Ok("Success");
        }



        /// <summary>
        /// Create role with certain name
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize("Admin")]
        [HttpPost("roles/create")]
        public async Task<IActionResult> CreateRole(RoleModel model)
        {
            await AccountService.CreateRole(model.RoleName);
            return Ok("Success");
        }
        

        /// <summary>
        /// Delete role by name
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize("Admin")]
        [HttpDelete("roles/delete")]
        public async Task<IActionResult> DeleteRole(RoleModel model)
        {
            await AccountService.DeleteRole(model.RoleName);
            return Ok("Success");
        }

        /// <summary>
        /// Get all roles
        /// </summary>
        /// <returns></returns>

        [Authorize(Roles ="admin")]
        [HttpGet("roles/get")]
        public async Task<IActionResult> GetRoles()
        {
            return Ok(await AccountService.GetRoles());
        }

        /// <summary>
        /// Assign roles to user
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize("admin")]
        [HttpPost("roles/assign")]
        public async Task<IActionResult> AssignUserToRole(AssignedRoles model)
        {
            await AccountService.AssignUserToRoles(new AssignedRoles
            {
                UserName = model.UserName,
                Roles = model.Roles
            });

            return Ok("Success");
        }
    }
}
