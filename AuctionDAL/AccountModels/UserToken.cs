﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AuctionDAL.AccountModels
{
    public class UserToken
    {
        public string Username { get; set; }
        public string Token { get; set; }
    }
}
