import { DecimalPipe } from "@angular/common";

export class User {
  userId: number;
  userName: string;
  email: string;
  firstName: string;
  middleName: string;
  lastName: string;
  phone: string;
  adress: string;



  constructor(userName: string, email: string, firstName: string, middleName: string, lastName: string, phone:string, adress: string) {
    this.userName = userName;
    this.email = email;
    this.firstName = firstName;
    this.middleName = middleName;
    this.lastName = lastName;
    this.phone = phone;
    this.adress = adress;
  }


}
