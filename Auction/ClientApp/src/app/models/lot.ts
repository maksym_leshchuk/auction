import { DecimalPipe } from "@angular/common";

export class Lot{
  lotId: number;
  userId: number;
    name: string;
    description:string;
    startingPrice: number;
    creationDate: Date;


  constructor( name: string, description: string, startingPrice: number) {
        
        this.name = name;
        this.description = description;
        this.startingPrice = startingPrice;
        
  }

  
}
