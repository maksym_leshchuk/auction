﻿using System;
using System.Collections.Generic;
using System.Text;
using AuctionBLL.Models;
using AuctionDAL.Models;
using AutoMapper;

namespace AuctionBLL
{



    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<User, UserModel>()
            //.ForMember(p => p.Phone, c => c.MapFrom(user => user.PersonalInformation.Phone))
            //.ForMember(p => p.Adress, c => c.MapFrom(user => user.PersonalInformation.Adress))
            .ReverseMap();


            
            CreateMap<Bid, BidModel>()
            .ReverseMap();

            CreateMap<Lot, LotModel>()
            .ReverseMap();

            
        }
    }
    
}
