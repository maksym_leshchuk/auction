import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { LotsListComponent } from './lots-list/lots-list.component';
import { LotDetailsComponent } from './lot-details/lot-details.component';
import { LotFormComponent } from './lot-form/lot-form.component';
import { BidFormComponent } from './bid-form/bid-form.component';
import { LoginComponent } from './login/login.component';
import { AuthorizationGuard } from './authorization-guard/authorization.guard';
import { ProfileComponent } from './profile/profile.component';
import { UserFormComponent } from './user-form/user-form.component';
import { AuthInterceptor } from './authorization-guard/auth.interceptor';
import { RegisterComponent } from './register/register.component';
import { LogoutComponent } from './logout/logout.component';
import { ErrorInterceptor } from './error/error-interceptor';
import { UserListComponent } from './user-list/user-list.component';
import { FooterComponent } from './footer/footer.component';


@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    LotsListComponent,
    LotDetailsComponent,
    LotFormComponent,
    BidFormComponent,
    LoginComponent,
    ProfileComponent,
    UserFormComponent,
    RegisterComponent,
    LogoutComponent,
    UserListComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([

      { path: 'login', component: LoginComponent },
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'lots', component: LotsListComponent },


      { path: 'profile', component: ProfileComponent, canActivate: [AuthorizationGuard] },
      { path: 'register', component: RegisterComponent },
      { path: 'logout', component: LogoutComponent, canActivate: [AuthorizationGuard]  },

      { path: 'user/edit', component: UserFormComponent },
      { path: 'users', component: UserListComponent, canActivate: [AuthorizationGuard] },
      { path: 'lots/create-lot', component: LotFormComponent, canActivate: [AuthorizationGuard]},
      { path: 'lots/:id', component: LotDetailsComponent, canActivate: [AuthorizationGuard]  }
      
    ])
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true,
    }

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
