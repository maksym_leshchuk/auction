import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { TokenService } from '../services/token.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'auction-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  users: User[];
  role: string;
  constructor(private _userService: UserService, private _tokenService: TokenService) { }

  ngOnInit() {
    this._userService.getAll().subscribe(lots => this.users = lots);
    

    this.role = this._tokenService.getToken()["http://schemas.microsoft.com/ws/2008/06/identity/claims/role"];
    
  }

  

  delete(name: string) {
    
    this._userService.delete(name).subscribe();

    this.users.splice(this.users.findIndex(x => x.userName == name), 1)

  }

}
